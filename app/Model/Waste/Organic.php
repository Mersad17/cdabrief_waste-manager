<?php

namespace App\Model\Waste;
use App\Model\Services\AbstractService;
use App\Model\ServicesInterface\ComposterInterface;
use App\Model\ServicesInterface\IncineratorInterface;

class Organic extends AbstractWaste 
{
    public function setService(AbstractService $service)
    {
        if (!($service instanceof IncineratorInterface) or !($service instanceof ComposterInterface)) {
            throw new \Exception('Error');
        }
        // elseif ($service instanceof IncineratorInterface){
        //     parent::setService($service);
            
        // }
            parent::setService($service);


    }
}