<?php
namespace App\Model\Waste;
use App\Model\Services\AbstractService;



abstract class AbstractWaste{
    protected float $quantity;
    protected ?AbstractService $service;


    public function __construct(float $quantity)
    {
        $this->quantity = $quantity;
        $this->service = null;
    }
    public function setService(AbstractService $service)
    {
        $this->service = $service;
    }

    public function getService(): ?AbstractService
    { 
        return $this->service;
    }
    public function isAssigned(): bool
    {
        return !is_null($this->service);
    }
 
    // public function getType(): ?AbstractService
    // {
    //     return str_replace([__NAMESPACE__ . '\\', 'Task'], '', get_class($this->service));
    // }

    /**
     * Get the value of quantity
     *
     * @return float
     */
    public function getQuantity(): float
    {
        return $this->quantity;
    }

    /**
     * Set the value of quantity
     *
     * @param float $quantity
     *
     * @return self
     */
    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }
}