<?php

namespace App\Model\Waste;
use App\Model\Services\AbstractService;
use App\Model\ServicesInterface\IncineratorInterface;


class Glass extends AbstractWaste 
{
    public function setService(AbstractService $service)
    {
        if (!($service instanceof IncineratorInterface)) {
            throw new \Exception('Error');
        }

        parent::setService($service);
    }
}