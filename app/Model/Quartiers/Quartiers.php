<?php

namespace App\Model\Quartiers;

use App\Model\Waste\AbstractWaste;
use App\Model\Services\AbstractService;


class Quartiers
{
    protected string $quartier;
    protected int $population;

    protected array $services;
    protected array $wastes;
    

    public function __construct(string $quartier, int $population)
    {
        $this->quartier = $quartier;
        $this->population = $population;
        $this->wastes = [];
        $this->services = [];

    }
  
    public function __toString()
    {
        return $this->quartier;
    }

    public function addServices(AbstractService $service)
    {
        $this->services[] = $service;

        return $this;
    }

    public function getServices(): array
    {
        return $this->services;
    }

    public function addWaste(AbstractWaste $waste)
    {
        if ($waste->isAssigned() && !in_array($waste->getService(), $this->services)) {
            throw new \Exception('Cannot add task assigned to someone not in the team to the project');
        }

        $this->wastes[] = $waste;

        return $this;
    }

    public function assignWaste(AbstractWaste $waste, AbstractService $service)
    {
        if (!in_array($waste, $this->wastes)) {
            throw new \Exception('Cannot assign');
        }

        if (!in_array($service, $this->services)) {
            throw new \Exception('Cannot assign ');
        }

        $waste->setService($service);
        $serviceOldCapacity = $service->getCapacity();
        $wasteOldQuantity = $waste->getQuantity();
        if ( $serviceOldCapacity < $wasteOldQuantity) {
            $serviceNewCapacity = 0;
            $wasteNewQuantity = $wasteOldQuantity - $serviceOldCapacity;
            $waste->setQuantity($wasteNewQuantity);
            
            $full = true;
        }elseif ($serviceOldCapacity >= $wasteOldQuantity) {
            $serviceNewCapacity = $serviceOldCapacity - $wasteOldQuantity;
            $wasteNewQuantity = 0;

        }
        $waste->setQuantity($wasteNewQuantity);
        $service->setCapacity($serviceNewCapacity);
        return $this;
    }
  
    // public function getDuration(): int
    // {
    //     $duration = 0;
    //     foreach ($this->tasks as $task) {
    //         $duration += $task->getDuration();
    //     }

    //     return $duration;
    // }

    // public function getDurationByTaskType(): array
    // {
    //     $taskTypes = [];

    //     foreach ($this->tasks as $task) {
    //         $type = $task->getType();
    //         if (!isset($taskTypes[$type])) {
    //             $taskTypes[$type] = 0;
    //         }
    //         $taskTypes[$type] += $task->getDuration();
    //     }

    //     return $taskTypes;
    // }

    // public function getDurationByMember(): array
    // {
    //     $memberTimes = [];

    //     foreach ($this->tasks as $task) {
    //         if (!$task->isAssigned()) {
    //             continue;
    //         }
    //         $member = $task->getMember()->__toString();
    //         if (!isset($memberTimes[$member])) {
    //             $memberTimes[$member] = 0;
    //         }
    //         $memberTimes[$member] += $task->getDuration();
    //     }

    //     return $memberTimes;
    // }
}
