<?php
namespace App\Model\Services;

abstract class AbstractService{
   

    protected $capacity;
  


    public function __construct($capacity)
    {
        $this->capacity = $capacity;
        

    }

    public function __toString()
    {
        return str_replace([__NAMESPACE__ . '\\', 'Services'], '', get_class($this));

    }
 

    /**
     * Get the value of capacity
     */
    public function getCapacity()
    {
        return $this->capacity;
    }

    /**
     * Set the value of capacity
     */
    public function setCapacity($capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }
    public function isFull(): bool
    {
        return !is_null($this->member);
    }
}